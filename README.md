# todolist



## Descripción

Este monorepo incluye el frontend, API, del proyecto TODOLIST

## Tabla de Contenidos

1. [Prerequisitos](#prerequisitos)
2. [Stack](#stack)
3. [Instalación](#instalacion)

## Prerequisitos


- [nvm](https://github.com/nvm-sh/nvm)

## Stack

- Node
- EmberJs 5.1.0
  `npm install -g ember-cli`
- PostgreSQL

## Installation


### Cliente
- El cliente corre en `http://localhost:4200/`

### Servidor
- El servidor corre en `http://localhost:3000/`

