const express = require('express');
const conectarDB = require('../config/db');
const cors = require("cors");
const apolloServer = require('../graphql/apolloServerSetup.js');

const app= express();


conectarDB();
app.use(cors())
// Definimos ruta principal 

/*app.get('/',(req,res)=>{
    res.send("Hola Mundo")
})*/

app.use(express.json());

app.use('/api/productos', require('../routes/producto'));

apolloServer.applyMiddleware({ app, path: '/graphql' });

app.listen(4000 , () => {
    console.log("El servidor esta corriendo perfectamente")
})