'use strict';

module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    uuid: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    salt: DataTypes.STRING,
    firstName: DataTypes.STRING,
    middleName: DataTypes.STRING,
    firstSurname: DataTypes.STRING,
    secondSurname: DataTypes.STRING,
    identificationType: DataTypes.STRING,
    identification: DataTypes.STRING,
    email: DataTypes.STRING,
    phone1: DataTypes.STRING,
    phone2: DataTypes.STRING,
    picture: DataTypes.STRING,
    recoverPassword: DataTypes.INTEGER,
    passwordRecoveryTimestamp: DataTypes.STRING,
    hash: DataTypes.STRING,
    createUser: DataTypes.STRING,
    updateUser: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    status: DataTypes.STRING,
    erased: DataTypes.INTEGER
  }, { underscored: true, timestamps: false });
  user.associate = models => {
    user.hasMany(models.userAssignment);
  };
  return user;
};
