
  const cnx = require("../connection");

  const sequelize = cnx.configDb;
  const { Op } = require("sequelize");
  //#endregion
  //#region "Inserts, Selects, Updates or Deletes"

  const createUser = (data) => {
    const salt = baseProcess.commonFunctions.genRandomSalt(128);
    let passwd = "";
    if (undefined !== data.pwd) {
      passwd = baseProcess.commonFunctions.getSecureHash(data.pwd, salt);
    } else {
      passwd = baseProcess.commonFunctions.getSecureHash("pass1234", salt);
    }
    const userInfo = {
      id: data.id,
      firstName: data.firstName,
      firstSurname: data.firstSurname,
      identification: data.identification,
      email: data.email,
      username: data.username,
      salt: salt,
      password: passwd,
      hash: `user${data.hash}`,
      createUser: "admin",
      createdAt: new Date(),
    };
    return user.create(userInfo);
  };
  const getLastHashId = () => {
    return sequelize.query("SELECT (MAX(id) + 1) AS id FROM zigo_schema.users", {
      type: sequelize.QueryTypes.SELECT,
    });
  };

  //#endregion
  module.exports = {
    createUser,
    getLastHashId,
  };
  