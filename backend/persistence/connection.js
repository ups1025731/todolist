const Sequelize = require('sequelize');
require('dotenv').config();

const env = {
  dbname: process.env.DB_NAME,
  dbuser: process.env.DB_USER,
  dbpass: process.env.DB_PASS,
  dbhost: process.env.DB_HOST,
  dbschema: process.env.DB_SCHEMA,
  dbsequelizemeta: process.env.DB_SEQUELIZE_META,
  seedExcel: process.env.SEED_EXCEL_FILE,
  publicSchema: process.env.BD_PUBLIC_SCHEMA
};

const configDb = new Sequelize(env.dbname, env.dbuser, env.dbpass, {
  host: env.dbhost,
  schema: env.dbschema,
  dialect: 'postgres',
  migrationStorageTableName: env.dbsequelizemeta,
  migrationStorageTableSchema: env.dbschema,
  logging: false,
});

module.exports = { configDb, env };
