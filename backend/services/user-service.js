  const userRepository = require("../persistence/repositories/user-repository");

  const createUser = (
    uuid,
    body,
    x = null,
    objHotel = null,
    objClient = null
  ) => {
    const userValidation = () => {
      return new Promise((resolve, reject) => {
        userRepository
          .findUsersFromOrigin({ code: body.code, uuid: uuid })
          .then((resp) => {
            if (undefined === resp) {
              reject(resp);
            } else {
              resolve(resp[0]);
            }
          });
      });
    };
    const lastUserId = () => {
      return new Promise((resolve, reject) => {
        userRepository.getLastHashId().then((res) => {
          if (undefined === res) {
            reject(undefined);
          } else {
            resolve({ id: res[0].id });
          }
        });
      });
    };
    const lastAdministrationId = () => {
      return new Promise((resolve, reject) => {
        administrationRepository.getLastAdministrationHashId().then((res) => {
          if (undefined === res) {
            reject(undefined);
          } else {
            resolve({ id: res[0].id });
          }
        });
      });
    };
    const lastUAHashId = () => {
      return new Promise((resolve, reject) => {
        userAssignmentRepository.getLastUAHashId().then((res) => {
          if (undefined === res) {
            reject(undefined);
          } else {
            resolve({ id: res[0].id });
          }
        });
      });
    };
    const role = () => {
      return new Promise((resolve, reject) => {
        roleRepository.getSpecificRole(body.role).then((res) => {
          if (undefined === res) {
            reject(undefined);
          } else {
            resolve(res[0]);
          }
        });
      });
    };
    return Promise.all([
      userValidation(),
      lastUserId(),
      lastAdministrationId(),
      lastUAHashId(),
      role(),
    ])
      .then(([uv, uid, aid, ua, role]) => {
        return new Promise((resolve, reject) => {
          let resultContent = {
            idUserResult: 0,
            idOwnerResult: 0,
            idAdministrationResult: 0,
          };
          if (undefined === uv || uv.role.name === "operator") {
            reject(undefined);
          } else {
            body.id = uid.id;
            body.hash =
              uid.id <= 9 ? `00${uid.id}` : uid.id <= 99 ? `0${uid.id}` : uid.id;
            userRepository.createUser(body).then((cu) => {
              if (undefined === cu) {
                resolve(undefined);
              } else {
                body.id = ua.id;
                body.hash =
                  ua.id <= 9 ? `00${ua.id}` : ua.id <= 99 ? `0${ua.id}` : ua.id;
                if (x === null) {
                  body.id = aid.id;
                  body.hash =
                    aid.id <= 9
                      ? `00${aid.id}`
                      : ua.id <= 99
                      ? `0${aid.id}`
                      : aid.id;
                  administrationRepository
                    .createAdministration(body)
                    .then((ca) => {
                      if (undefined === ca) {
                        reject(undefined);
                      } else {
                        body.id = ua.id;
                        body.hash =
                          ua.id <= 9
                            ? `00${ua.id}`
                            : ua.id <= 99
                            ? `0${ua.id}`
                            : ua.id;
                        body.userId = cu.id;
                        body.ownerId = 1;
                        body.roleId = role.id;
                        body.administrationId = ca.id;
  
                        userAssignmentRepository.createUA(body).then((cua) => {
                          if (undefined === cua) {
                            reject(undefined);
                          } else {
                            resultContent.idUserResult = cu.id;
                            resultContent.idOwnerResult = 1;
                            resultContent.idAdministrationResult = ca.id;
                            resultContent.idUAResult = cua.id;
                            resultContent.uuid = cu.uuid;
                            resultContent.roleAlias = role.alias;
                            resolve(resultContent);
                          }
                        });
                      }
                    });
                } else {
                  if (x === "hotel") {
                    body.userId = cu.id;
                    body.ownerId = 1;
                    body.hotelId =
                      objHotel.hotel.id !== undefined
                        ? objHotel.hotel.id
                        : body.idHotel;
                    body.roleId = 3;
                    body.code = CONFIG_CODE_HOTEL_OWNER.toLowerCase();
                  } else if (x === "client") {
                    body.userId = cu.id;
                    body.ownerId = 1;
                    body.companyId = objClient.id;
                    body.roleId = 4;
                    body.code = CONFIG_CODE_CLIENT_OWNER.toLowerCase();
                  }
                  if (body.companies) {
                    userAssignmentRepository.updateUA(body);
                  }
  
                  userAssignmentRepository.createUA(body).then((cua) => {
                    if (undefined === cua) {
                      reject(undefined);
                    } else {
                      resultContent.idUserResult = cu.id;
                      resultContent.idUAResult = cua.id;
                      const payload = {
                        ownerUser: `${body.firstName} ${body.firstSurname}`,
                        tradename: body.tradename,
                        username: body.username,
                        password: body.pwd,
                        REDIRECT_VIEW_DETAIL_TEMPLATE:
                          REDIRECT_VIEW_DETAIL_TEMPLATE,
                      };
                      if (x === "hotel") {
                        sendEmail({
                          template_id: EMAIL_USER_HOTEL_ADMINISTRATION.template,
                          subject: EMAIL_USER_HOTEL_ADMINISTRATION.subject,
                          recipients: [{ email: body.email }],
                          dynamic_template_data: payload,
                        });
                      } else if (x === "client") {
                        sendEmail({
                          template_id: EMAIL_USER_CLIENT_ADMINISTRATION.template,
                          subject: EMAIL_USER_CLIENT_ADMINISTRATION.subject,
                          recipients: [{ email: body.email }],
                          dynamic_template_data: payload,
                        });
                      }
                      resolve(resultContent);
                    }
                  });
                }
              }
            });
          }
        });
      })
      .catch((err) => console.log(`Error in executing ${err}`));
  };

  module.exports = {
    createUser,
  };