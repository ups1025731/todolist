const mongoose = require('mongoose');

const ProductoSchema = mongoose.Schema({
    nombres: {
        type: String,
        required: true
    },
    apellidos: {
        type: String,
        required: true
    },
    direccion: {
        type: String,
        required: true
    },
    fecha_nac: {
        type: Date,
        required: true
    },
    cuenta: {
        type: Boolean,
        required: true
    },
    hijos: {
        type: Number,
        required: true
    },
    saldo: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Producto', ProductoSchema);