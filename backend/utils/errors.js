class TokenExpiredError extends Error {
    constructor(message) {
        super(message);
        this.name = 'TokenExpiredError';
        this.code = '401';
    }
}


class ValidationError extends Error {
    constructor(message) {
        super(message);
        this.name = 'ValidationError';
        this.code = '522';
    }
}

class RoomUnAvailableError extends Error {
    constructor(message, data = {}) {
        super(message);
        this.name = 'RoomUnAvailableError';
        this.code = '523';
        this.data = data;
    }
}

module.exports = {
    TokenExpiredError,
    ValidationError,
    RoomUnAvailableError
};
