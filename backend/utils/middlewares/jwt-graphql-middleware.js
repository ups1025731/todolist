const jwt = require('jsonwebtoken');

const jwtConfig = require('../configs/jwt-config');
const errors = require('../errors');

const { TokenExpiredError } = errors;

const authGraphql = ((req) => {
  if (typeof req.headers.authorization === 'undefined') {
    throw new Error('No existe cabecera de autenticación.');
  }
  const token = req.headers.authorization;
  const tokenArray = token.split(' ');
  const jwtToken = tokenArray[1];

  if (!jwtToken) {
    throw new Error('Token no enviado.');
  }
  return jwt.verify(jwtToken, jwtConfig.secret, (err, decoded) => {
    if (err) {
      throw new TokenExpiredError('Invalid Token');
    }

    return decoded;
  });
});

module.exports = authGraphql;
