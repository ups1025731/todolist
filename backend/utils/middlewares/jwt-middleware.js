const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const jwtConfig = require('../configs/jwt-config');
const status = require('http-status');

const baseProcess = require('utils/base-process');
const logger = baseProcess.logger;

const nonSecurePaths = [
  '/api/healthcheck',
  '/api/info',
  '/api/auth/login',
  '/api/landing/sendMessage',
  '/api/auth/forgot',
  '/api/auth/recovery',
  '/api/auth/url-validation',
  '/api/batch/quiz',
  '/api/batch/pdf',
  '/api/reservation/confirmAvailability',
  '/api/batch/reminder',
  '/api/batch/reservation-pending',
];

router.use((req, res, next) => {

  const isSecuredPath = (nonSecurePaths.findIndex(r => req.path.includes(r)) < 0);

  if (isSecuredPath) {

    const token = req.headers.authorization || null;

    if (!token) {
      res.status(status.UNAUTHORIZED).send({ message: 'Token no enviado.' });
    } else {

      const tokenArray = token.split(' ');
      const jwtToken = tokenArray[1];

      if (!jwtToken) {
        res.status(status.UNAUTHORIZED).send({ message: 'Formato de token invalido.' });
      } else {

        jwt.verify(jwtToken, jwtConfig.secret, (err, decoded) => {
          if (err) {
            res.status(status.UNAUTHORIZED).json({ message: 'Token invalido' });
            logger.error(err);
          } else {
            req.decoded = decoded;
          }
        });
      }
    }
  }

  next();

});

module.exports = router;
