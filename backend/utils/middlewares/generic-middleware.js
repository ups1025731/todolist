const genericMiddleware = (body) => {
    if (typeof body === 'string' || typeof body === 'number' || typeof body === 'function' || typeof body === 'undefined') {
      return {
        result: body
      };
    }
    if (typeof body === 'object') {
      if (typeof body.dataValues !== 'undefined') {
        delete body.dataValues.password;
        delete body.dataValues.salt;
        delete body.dataValues.createdAt;
        delete body.dataValues.createUser;
      }
      return {
        result: body
      };
    }
    body.forEach((p) => {
      delete p.dataValues.password;
      delete p.dataValues.salt;
      delete p.dataValues.createdAt;
      delete p.dataValues.updatedAt;
    });
    return { result: body };
  };
  
  module.exports = genericMiddleware;
  