const commonDateFunctions = require('./common-date-functions/common-date-functions');
const commonFunctions = require('./common-functions/common-functions');
const logger = require('./configs/logger-config');
const redisCache = require('./redis/redisCache');
const redisPublisher = require('./redis/redisPublisher');

let baseProcess = function () { };

baseProcess.prototype.commonDateFunctions = commonDateFunctions;
baseProcess.prototype.commonFunctions = commonFunctions;
baseProcess.prototype.logger = logger;
baseProcess.prototype.redisCache = redisCache;
baseProcess.prototype.redisPublisher = redisPublisher;
baseProcess = new baseProcess();

module.exports = exports = baseProcess;
