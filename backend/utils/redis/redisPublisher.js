require('dotenv').config();

const redis = require('redis');
const logger = require('../configs/logger-config');

const { REDIS_HOST: host, REDIS_PORT: port, REDIS_PASSWORD: password } = process.env;

const client = redis.createClient({
    url: `redis://:${password}@${host}:${port}`
});

(async function (client) {
    await client.connect();
})(client);

client.on('error', function (error) {
    logger.error(`RedisPublisher no se pudo connectar al servidor REDIS: ${error}`);
});

(function (redisPublisher) {
    redisPublisher.publish = async (channel, message) => {
        try {
            await client.xAdd(channel, '*', { data: JSON.stringify(message) });
            logger.info(`Redis Publish Message: ${JSON.stringify({ channel, message })}`);
        } catch (error) {
            logger.error(`Redis Publish Message Error ${JSON.stringify({ channel, message })} ~ ${error}`);
        }
    };
})(typeof exports === 'undefined' ? redisPublisher = {} : exports);
