require('dotenv').config();

const redis = require('redis');
const logger = require('../configs/logger-config');

const { ENABLE_REDIS_CACHE: enableRedisCache, REDIS_HOST: host, REDIS_PORT: port, REDIS_PASSWORD: password } = process.env;

const client = redis.createClient({
    url: `redis://:${password}@${host}:${port}`
});

client.on('error', function (error) {
    if ('1' === enableRedisCache) {
        logger.error(`RedisCache no se pudo connectar al servidor REDIS: ${error}`);
    }
});

(async function (client) {
    if ('1' === enableRedisCache) {
        await client.connect();
    }
})(client);


(function (redisCache) {
    redisCache.getCache = async (idCache) => {
        try {
            if ('1' === enableRedisCache) {
                const replay = await client.get(idCache);
                if (replay) {
                    logger.info(`ID Cache ${idCache} retorna información exitosamente`);
                    return JSON.parse(replay);
                }
                logger.info(`ID Cache ${idCache} retorna un valor nulo`);
            }
            return null;
        } catch (error) {
            logger.error(`redisCache.getCache, no pudo retornar el cache con ID ${idCache} ~ ${error}`);
            return null;
        }
    };

    redisCache.setCache = async (idCache, data) => {
        try {
            if ('1' === enableRedisCache) {
                await client.set(idCache, JSON.stringify(data));
                logger.info(`ID Cache ${idCache} guardado exitosamente`);
            }
            return data;
        } catch (error) {
            logger.error(`redisCache.setCache, no pudo retornar el cache con ID ${idCache} ~ ${error}`);
            return null;
        }
    };

    redisCache.delCache = async (idCache) => {
        try {
            if ('1' === enableRedisCache) {
                const delReplay = await client.del(idCache);
                if (delReplay === 1) {
                    logger.info(`ID Cache ${idCache} eliminado exitosamente`);
                } else {
                    logger.error(`redisCache.delCache, no se pudo eliminar el cache con ID ${idCache}`);
                }
            }
            return null;
        } catch (error) {
            logger.error(`redisCache.delCache, no se pudo eliminar el cache con ID ${idCache} ~ ${error}`);
            return null;
        }
    };
})(typeof exports === 'undefined' ? redisCache = {} : exports);
