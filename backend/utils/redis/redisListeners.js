import { REDIS_EVENTS } from './../constants';

const reservationRepository = require('./../../persistence/repositories/reservation-repository');
const reservationSalonRepository = require('./../../persistence/repositories/reservation-salon-repository');
const redisSubscriber = require('./redisSubscriber');

const init = async () => {
  redisSubscriber.subscribe(REDIS_EVENTS.AckReservation, ({ reservationId, userUuid }) => reservationRepository.sendToBilling(reservationId, userUuid));
  redisSubscriber.subscribe(REDIS_EVENTS.AckReservationSalon, ({ reservationId }) => reservationSalonRepository.sendToBilling(reservationId));
};

module.exports = {
  init,
};
