// https://github.com/redis/node-redis/blob/master/examples/stream-consumer-group.js
// https://github.com/redis/node-redis/blob/master/examples/stream-consumer.js

require('dotenv').config();

const redis = require('redis');
const logger = require('../configs/logger-config');

const { REDIS_HOST: host, REDIS_PORT: port, REDIS_PASSWORD: password } = process.env;
const CONSUMER_GROUP = 'server';
const CONSUMER_NAME = 'consumer_1';

const client = redis.createClient({
    url: `redis://:${password}@${host}:${port}`
});

(async function (client) {
    await client.connect();
})(client);

client.on('error', function (error) {
    logger.error(`redisSubscriber no se pudo connectar al servidor REDIS: ${error}`);
});

(function (redisSubscriber) {
    const createGroup = async (channel, groupName) => {
        try {
            await client.xGroupCreate(channel, groupName, '0', {
                MKSTREAM: true
            });
        } catch (error) {
            logger.error(`RedisSubscriber Consumer Group ${groupName} already exists`);
        }
    };

    redisSubscriber.subscribe = async (channel, callback) => {
        await createGroup(channel, CONSUMER_GROUP);

        while (true) {
            try {
                const events = await client.xReadGroup(
                    redis.commandOptions({
                        isolated: true
                    }),
                    CONSUMER_GROUP,
                    CONSUMER_NAME,
                    [{
                        key: channel,
                        id: '>'
                    }],
                    {
                        count: 1,
                        BLOCK: 10000
                    }
                );

                if (events) {
                    // const entryId = events[0].messages[0].id;
                    const message = events[0].messages[0].message;
                    // await client.xAck(channel, CONSUMER_GROUP, entryId);
                    logger.info(`RedisSubscriber Event Received: ${JSON.stringify({ channel, events })}`);
                    if (callback) {
                        callback(message);
                    }
                } else {
                    logger.info(`RedisSubscriber no new events in channel ${channel}`);
                }

            } catch (error) {
                logger.error(`RedisSubscriber Error ${JSON.stringify({ channel })} ~ ${error}`);
            }
        }
    };

})(typeof exports === 'undefined' ? redisSubscriber = {} : exports);
