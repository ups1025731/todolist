const moment = require('moment');

(function (commonDateFunctions) {
  commonDateFunctions.diffHours = (oldTimestampDate, futureTimestampDate) => {
    let diff = (oldTimestampDate - futureTimestampDate) / 1000;
    diff /= (60 * 60);
    return Math.abs(Math.round(diff));
  };
  commonDateFunctions.chkInChkOutCustom = dateTimeToFormat => {
    moment.locale('es');
    let month = moment(dateTimeToFormat).format('MMMM');
    month = month.replace('.', '');
    month = month.substring(0, 3);
    return `${moment(dateTimeToFormat).format('DD')} de ` +
      `${month}. de ${moment(dateTimeToFormat).format('YYYY')}`;
  };
  commonDateFunctions.timeFormatTemplate = dateTimeToFormat => {
    moment.locale('es');
    return `${moment(dateTimeToFormat).format('hh:mm A')}`;
  };
  commonDateFunctions.currentDateTimeInStr = dateTimeToFormat => {
    moment.locale('es');
    return `${moment(dateTimeToFormat).format('YYYY-MM-DD HH:mm:ss.fff')}`;
  };
})(typeof exports === 'undefined' ? commonDateFunctions = {} : exports);