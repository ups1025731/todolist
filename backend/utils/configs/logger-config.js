const winston = require('winston');
const moment = require('moment-timezone');
const { combine, timestamp, label, printf } = winston.format;

const myFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});
const dateTimeZoneFormat = () => {
  return moment().tz('America/Guayaquil').format('YYYY-MM-DD HH:mm:ss.SSS');
};
const options = {
  file: {
    filename: `${__dirname}../../../logs/app.log`,
    handleExceptions: true,
    json: true,
    maxsize: 5242880,
    maxFiles: 5,
    colorize: false,
  },
  console: {
    handleExceptions: true,
    json: false,
    colorize: true,
  }
};

const logger = winston.createLogger({
    format: combine(
      label({ label: process.env.NODE_ENV === 'development' ? 'app-test' : 'PRD' }),
      timestamp({ format: dateTimeZoneFormat }),
      myFormat
    ),
    transports:
      [
        new winston.transports.File(options.file),
        new winston.transports.Console(options.console),
      ],
    exitOnError:
      false,
  });

module.exports = logger;
