const Sentry = require('@sentry/node');
const Transport = require('winston-transport');

// TODO: Remove this classs, integrate with azure appinsigths instead
module.exports = class SentryTransport extends Transport {
  constructor(opts) {
    super(opts);
  }

  log(info, callback) {
    setImmediate(() => {
      this.emit('logged', info);
    });

    if (callback) {
      callback();
    }

    if (info instanceof Error) {
      Sentry.captureException(info);
    }
  }
};
