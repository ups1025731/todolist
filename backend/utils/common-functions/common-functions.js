const crypto = require('crypto');
const moment = require('moment');
const path = require('path');
const url = require('url');
const fs = require('fs');

require('dotenv').config();
(function (commonFunctions) {
  function regexEmailValidation(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  function userPwdRegexValidation(userPwd) {
    const usrPwdExp = /^(?=.*[0-9])(?=.*[a-z])[a-z0-9]{6,}$/;
    if (!(userPwd.length >= 6 && userPwd.length <= 9)) {
      return false;
    }
    return usrPwdExp.test(String(userPwd));
  }
  function fieldsForMassiveHotel() {
    return [
      'id',
      'owner_id',
      'unique_hotel_id',
      'tradename',
      'type_establishment',
      'number_stars',
      'city_catalog',
      'group',
      'images',
      'people_capacity',
      'bed_capacity',
      'municipal_tax',
      'service',
      'sure',
      'description',
      'contact_phone',
      'contact_email',
      'reservations_manager_name',
      'reservations_manager_last_name',
      'reservations_manager_phones',
      'reservations_manager_emails',
      'accounting_manager_name',
      'accounting_manager_last_name',
      'accounting_manager_phones',
      'accounting_manager_emails',
      'events_manager_name',
      'events_manager_last_name',
      'events_manager_phones',
      'events_manager_emails',
      'address',
      'latitude',
      'longitude',
      'payment_business_name',
      'payment_ruc',
      'payment_legal_representative',
      'payment_ci',
      'bank_name',
      'account_type',
      'account_number',
      'payment_email',
      'additional_text',
      'logo',
      'all_public',
      'companies_only',
      'some_companies',
      'hash',
      'create_user',
      'update_user',
      'created_at',
      'updated_at',
      'status',
      'erased'
    ];
  }
  function fieldsForMassiveClient() {
    return [
      'id',
      'owner_id',
      'config_catalog_code_customer_type',
      'creation_date',
      'business_name',
      'tradename',
      'ruc',
      'phone',
      'email',
      'address',
      'catalog_code_city',
      'seller',
      'industry',
      'preference',
      'business_contact_name',
      'business_contact_lastname',
      'business_contact_phone',
      'business_contact_email',
      'accounting_name',
      'accounting_lastname',
      'accounting_phone',
      'accounting_email',
      'day_approved_credit',
      'purchase_order',
      'send_backup_invoice_pdf',
      'send_physical_invoice',
      'physical_delivery_address',
      'person_who_receives_invoice',
      'enable_approval',
      'first_name',
      'first_surname',
      'identification',
      'username',
      'user_email'
    ];
  }
  function getSequentialPadLeftNumber(num, padLeft) {
    let result = '';
    const realValue = num * 1;
    switch (padLeft) {
      case 3:
        result = realValue <= 9 ? `00${realValue}` : (realValue <= 99 ? `0${realValue}` : realValue);
        break;
      case 4:
        result = realValue <= 9 ? `00${realValue}` : (realValue <= 99 ? `0${realValue}` : realValue);
        if (realValue <= 9) {
          result = `000${realValue}`;
        } else if (realValue >= 10 && realValue <= 99) {
          result = `00${realValue}`;
        } else if (realValue >= 100 && realValue <= 999) {
          result = `0${realValue}`;
        } else {
          result = realValue;
        }
        break;
      case 2:
        result = realValue <= 9 ? `0${realValue}` : realValue;
        break;
    }
    return result;
  }
  function rucCiContentValidation(num, len) {
    let rucCi = '';
    if (num.length === len) {
      for (let i = 0; i < num.length; i += 1) {
        if (Number.isInteger(parseInt(num.charAt(i)))) {
          rucCi += num.charAt(i);
        } else {
          return null;
        }
      }
      return rucCi;
    }
    return null;
  }
  function numberAmountValidation(num) {
    let newNum = '';
    for (let i = 0; i < num.length; i += 1) {
      if (Number.isInteger(parseInt(num.charAt(i)))) {
        newNum += num.charAt(i);
      } else {
        return null;
      }
    }
    return newNum;
  }
  function regexOnlyTextValidation(text) {
    const re = /^([a-zA-ZÀ-ú]+\s)*[a-zA-ZÀ-ú]+$/;
    return re.test(text);
  }
  function identityValidation(identity) {
    if (identity.length === 10) {
      let digit = identity.split('').map(Number);
      let codeProv = digit[0] * 10 + digit[1];
      if (codeProv >= 1 && (codeProv <= 24 || codeProv === 30)) {
        let digitCheck = digit.pop();
        let digitMath = digit.reduce((beforeValue, currValue, index) => {
          return beforeValue - (currValue * (2 - index % 2)) % 9 - (currValue === 9) * 9;
        }, 100) % 10;
        return digitMath === digitCheck;
      }
    }
    return false;
  }
  function identityValidationFinal(identity) {
    try {
      // jshint maxdepth:5
      if (identity.length === 10) {
        const first2Digits = parseInt(identity.substring(0, 2), 10);
        if ((first2Digits < 25 && first2Digits > 0) || first2Digits === 30) {
          let total = 0;
          for (let i = 0; i < 9; i += 1) {
            const currD = parseInt(identity.substring(i, i + 1), 10);
            total += (i % 2 === 0) ? ((currD * 2 > 9) ?
              currD * 2 - 9 : currD * 2) : (parseInt(identity.substring(i, i + 1), 10));
          }
          total = total % 10;
          total = (total === 0) ? 0 : 10 - total;
          if (total === parseInt(identity.substring(identity.length - 1, identity.length), 10)) {
            return true;
          }
        }
      }
    } catch (err) {
      console.log('<=== identityValidationFinal failed ===>', err);
    }
    return false;
  }
  function upperCaseAllFirstLetter(textStr) {
    const textSplit = textStr.replace(/  +/g, ' ').split(' ');
    let result = '';
    for (let _i = 0; _i < textSplit.length; _i+=1) {
      result += `${textSplit[_i].charAt(0).toUpperCase()}${textSplit[_i].slice(1)} `;
    }
    return result.trim();
  }
  function myLocalTimeStr() {
    return moment().format('dddd, MMMM DD YYYY :: HH:mm:ss.SSS');
  }
  commonFunctions.validationEmail = email => {
    return regexEmailValidation(email);
  };
  commonFunctions.genRandomSalt = (length) => {
    return crypto.randomBytes(Math.ceil(length / 2))
      .toString('hex')
      .slice(0, length);
  };
  commonFunctions.getSecureHash = (password, salt) => {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    return hash.digest('hex');
  };
  commonFunctions.fullUrl = (req) => {
    return url.format({
      protocol: req.protocol,
      host: req.get('host'),
      pathname: req.originalUrl
    });
  };
  commonFunctions.rsaEncrypt = data => {
    const absPath = path.resolve(__dirname + '/../keys/publicKey.pem');
    const pub = fs.readFileSync(absPath, 'utf-8');
    const buffer = Buffer.from(data.toString());
    const encrypted = crypto.publicEncrypt(pub, buffer);
    return encrypted.toString('base64');
  };
  commonFunctions.rsaDecrypt = data => {
    const absPath = path.resolve(__dirname + '/../keys/privateKey.pem');
    const priv = fs.readFileSync(absPath, 'utf-8');
    const buffer = Buffer.from(data.toString('base64'), 'base64');
    const decrypted = crypto.privateDecrypt({
      key: priv,
      passphrase: process.env.RSA_PASSPHRASE
    }, buffer);
    return decrypted.toString('utf-8');
  };
  commonFunctions.generateReference = () => {
    function S4() {
      return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }
    return S4() + S4() + S4();
  };
  commonFunctions.templateChkInChkOut = (chkIn, chkOut) => {
    return `<!--[if mso | IE]>
<table role="presentation" border="0" cellpadding="0" cellspacing="0">
   <tr>
      <td class="" style="vertical-align:bottom;width:280px;">
         <![endif]-->
         <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;">
            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:bottom;" width="100%">
               <tr>
                  <td align="left" style="font-size:0px;padding:10px 25px;padding-top:20px;word-break:break-word;">
                     <div style="font-family:Karla, Tahoma, Arial, sans-serif;font-size:15px;line-height:18px;text-align:left;color:#464C4F;">
                     <b>Check-in: </b>${chkIn}</div>
                  </td>
               </tr>
            </table>
         </div>
         <!--[if mso | IE]>
      </td>
      <td class="" style="vertical-align:bottom;width:280px;">
         <![endif]-->
         <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;">
            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:bottom;" width="100%">
               <tr>
                  <td align="right" class="mobLeft" style="font-size:0px;padding:10px 25px;word-break:break-word;">
                     <div style="font-family:Karla, Tahoma, Arial, sans-serif;font-size:15px;line-height:18px;text-align:right;color:#464C4F;">
                     <b>Check-out: </b>${chkOut}</div>
                  </td>
               </tr>
            </table>
         </div>
         <!--[if mso | IE]>
      </td>
      <td class="" style="vertical-align:bottom;width:560px;">
         <![endif]-->
         <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;">
            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:bottom;" width="100%">
               <tr>
                  <td align="left" style="font-size:0px;padding:10px 25px;padding-bottom:20px;word-break:break-word;">
                     <div style="font-family:Karla, Tahoma, Arial, sans-serif;font-size:15px;line-height:18px;text-align:left;color:#464C4F;">
                     <b>Hora de llegada: </b>12:00 AM</div>
                  </td>
               </tr>
            </table>
         </div>
         <!--[if mso | IE]>
      </td>
   </tr>
</table>
<![endif]-->`;
  };
  commonFunctions.contactPhoneValidation = (phoneArray, field) => {
    let result = { status: false, phone: `Incompleto, \"<strong>${field}</strong>\" no cumple el mínimo requerido` };
    let phones = '';
    for (let i = 0; i < phoneArray.length; i += 1) {
      if (!(phoneArray[i].length === 10 || phoneArray[i].length === 9)) {
        return result;
      }
      phones += phoneArray[i] + ';';
    }
    result.status = true;
    result.phone = phones.length > 1 ? phones.substring(0, phones.length - 1) : phones;
    return result;
  };
  commonFunctions.contactEmailValidation = (emailArray, field) => {
    let result = { status: false, email: `No válido, \"<strong>${field}</strong>\" contiene errores en su contenido` };
    let emails = '';
    for (let i = 0; i < emailArray.length; i += 1) {
      if (!regexEmailValidation(emailArray[i])) {
        return result;
      }
      emails += emailArray[i] + ';';
    }
    result.status = true;
    result.email = emailArray.length === 1 ? emails.substring(0, emails.length - 1) : emails;
    return result;
  };
  commonFunctions.floatFieldValidation = (floatNumber, field) => {
    let result = {
      status: false,
      floatNumber: `No válido, \"<strong>${field}</strong>\" contiene errores en su contenido`
    };
    const content = parseFloat(floatNumber);
    if (isNaN(content)) {
      return result;
    }
    result.status = true;
    result.floatNumber = content;
    return result;
  };
  commonFunctions.partialRucValidation = (ruc, field) => {
    let result = { status: false, ruc: `No válido, \"<strong>${field}</strong>\" contiene errores en su contenido` };
    const rucVal = rucCiContentValidation(ruc, 13);
    if (!!rucVal) {
      result.status = true;
      result.ruc = rucVal;
      return result;
    }
    return result;
  };
  commonFunctions.partialCiValidation = (ci, field) => {
    let result = { status: false, ci: `No válido, \"<strong>${field}</strong>\" contiene errores en su contenido` };
    if (ci.length !== 10) {
      return result;
    }
    result.status = true;
    result.ci = ci;
    return result;
  };
  commonFunctions.userPwdValidation = (usrPwd, field) => {
    let result = {
      status: false,
      usrPwd: `No válido, \"<strong>${field}</strong>\" no cumple con validaciones requeridas. Min. 6, max. 9 y sólo letras minusculas y combinado con números`
    };
    if (!userPwdRegexValidation(usrPwd)) {
      return result;
    }
    result.status = true;
    result.usrPwd = usrPwd;
    return result;
  };
  commonFunctions.fieldMassiveHotelComparision = arrayExcel => {
    let result = {
      status: false,
      message: `Contenido de archivo excel no válido`,
      detail: []
    };
    const fields = fieldsForMassiveHotel();
    if (Object.keys(arrayExcel).length !== fields.length) {
      result.detail.push('Longitud de campos incorrecta');
    }
    for (let [key] of Object.entries(arrayExcel)) {
      if ('__EMPTY_1' !== key && '__EMPTY' !== key) {
        const findResult = fields.indexOf(key);
        if (findResult === -1) {
          result.detail.push(`El campo '${key}' no corresponde a lo requerido`);
        }
      }
    }
    if (result.detail.length === 0) {
      result.status = true;
      result.message = 'OK';
    }
    return result;
  };
  commonFunctions.sequentialHotelNumber = sequential => {
    let codeBase = 'ZIG__D__HT__U__';
    let currDec = 0;
    let currUni = 0;
    const prevUni = sequential.split('HT');
    const prevDec = prevUni[0].split('ZIG');
    if ((prevUni[1] * 1) === 999) {
      currUni = 1;
      currDec = (prevDec[1] * 1) + 1;
    } else {
      currUni = (prevUni[1] * 1) + 1;
      currDec = prevDec[1] * 1;
    }
    if ((prevDec[1] * 1) === 99) {
      currDec = 1;
    }
    const dec = getSequentialPadLeftNumber(currDec, 2);
    const uni = getSequentialPadLeftNumber(currUni, 3);
    return codeBase.replace('__D__', dec).replace('__U__', uni);
  };
  commonFunctions.assembleMassiveHotelReport = data => {
    let template = `<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border:1px solid #ccc;vertical-align:top;" width="100%">
              <tr>
                <td align="left" style="font-size:0px;padding:0px 25px;word-break:break-word;">
                  <div style="font-family:Karla, Tahoma, Arial, sans-serif;font-size:15px;line-height:22px;text-align:left;color:#464C4F;">
                  <h3 style="text-align: center">ID Excel - ___NUMBERID___</h3></div>
                </td>
              </tr>
              <tr>
                <td align="left" style="font-size:0px;padding:0;word-break:break-word;">
                  <table cellpadding="0" cellspacing="0" width="100%" border="0" style="color:#464C4F;font-family:Karla, Tahoma, Arial, sans-serif;font-size:15px;line-height:22px;table-layout:auto;width:100%;border:1px solid #ccc;">
                    <tr style="text-align:left;padding: 0;">
                      <th style="border-top: 1px solid #ccc;padding: 10px 8px;" width="20%">ID Hotel:</th>
                      <td style="border: 1px solid #ccc;padding: 10px 15px;border-right: 0">___UNIQUEHOTELID___</td>
                    </tr>
                    <tr>
                      <th style="border-top: 1px solid #ccc;padding: 10px 8px;text-align:left" width="20%">Hotel:</th>
                      <td style="border: 1px solid #ccc;padding: 10px 15px;">___TRADENAME___</td>
                    </tr>
                    <tr>
                      <th style="border-top: 1px solid #ccc;padding: 10px 8px;text-align:left" width="20%">Informe:</th>
                      <td style="border: 1px solid #ccc;padding: 10px 15px;border-bottom: 0"> ___DESCRIPTION___</td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>`;
    let description = '';
    for (let i = 0; i < data.issueDetail.length; i += 1) {
      description += `<li>${data.issueDetail[i]}</li>`;
    }
    return template
      .replace('___DESCRIPTION___', description)
      .replace('___TRADENAME___', data.hotelName)
      .replace('___UNIQUEHOTELID___', data.uniqueHotelId)
      .replace('___NUMBERID___', data.hotelId);
  };
  commonFunctions.fieldMassiveClientComparision = arrayExcel => {
    let result = {
      status: false,
      message: `Contenido de archivo excel no válido`,
      detail: []
    };
    const fields = fieldsForMassiveClient();
    if (Object.keys(arrayExcel).length !== fields.length) {
      result.detail.push('Longitud de campos incorrecta');
    }
    for (let [key] of Object.entries(arrayExcel)) {
      const findResult = fields.indexOf(key);
      if (findResult === -1) {
        result.detail.push(`El campo '${key}' no corresponde a lo requerido`);
      }
    }
    if (result.detail.length === 0) {
      result.status = true;
      result.message = 'OK';
    }
    return result;
  };
  commonFunctions.partialCIValidation = (ci, field) => {
    let result = { status: false, ci: `No válido, \"<strong>${field}</strong>\" contiene errores en su contenido` };
    const ciVal = rucCiContentValidation(ci, 10);
    if (null !== ciVal) {
      result.status = true;
      result.ci = ciVal;
      return result;
    }
    return result;
  };
  commonFunctions.numberValidation = (number, field) => {
    let result = { status: false, number: `No válido, \"<strong>${field}</strong>\" contiene errores en su contenido` };
    const num = numberAmountValidation(number);
    if (null !== num) {
      result.status = true;
      result.number = num;
      return result;
    }
    return result;
  };
  commonFunctions.assembleMassiveClientReport = data => {
    let template = `<table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border:1px solid #ccc;vertical-align:top;" width="100%">
              <tr>
                <td align="left" style="font-size:0px;padding:0px 25px;word-break:break-word;">
                  <div style="font-family:Karla, Tahoma, Arial, sans-serif;font-size:15px;line-height:22px;text-align:left;color:#464C4F;">
                  <h3 style="text-align: center">ID Excel - ___NUMBERID___</h3></div>
                </td>
              </tr>
              <tr>
                <td align="left" style="font-size:0px;padding:0;word-break:break-word;">
                  <table cellpadding="0" cellspacing="0" width="100%" border="0" style="color:#464C4F;font-family:Karla, Tahoma, Arial, sans-serif;font-size:15px;line-height:22px;table-layout:auto;width:100%;border:1px solid #ccc;">
                    <tr style="text-align:left;padding: 0;">
                      <th style="border-top: 1px solid #ccc;padding: 10px 8px;" width="20%">ID Cliente:</th>
                      <td style="border: 1px solid #ccc;padding: 10px 15px;border-right: 0">___RUC___</td>
                    </tr>
                    <tr>
                      <th style="border-top: 1px solid #ccc;padding: 10px 8px;text-align:left" width="20%">Cliente:</th>
                      <td style="border: 1px solid #ccc;padding: 10px 15px;">___TRADENAME___</td>
                    </tr>
                    <tr>
                      <th style="border-top: 1px solid #ccc;padding: 10px 8px;text-align:left" width="20%">Informe:</th>
                      <td style="border: 1px solid #ccc;padding: 10px 15px;border-bottom: 0"> ___DESCRIPTION___</td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>`;
    let description = '';
    for (let i = 0; i < data.issueDetail.length; i += 1) {
      description += `<li>${data.issueDetail[i]}</li>`;
    }
    return template
      .replace('___DESCRIPTION___', description)
      .replace('___TRADENAME___', data.businessName)
      .replace('___RUC___', data.ruc)
      .replace('___NUMBERID___', data.clientId);
  };
  commonFunctions.textOnlyValidation = (txt, field) => {
    let result = {
      status: false,
      onlyText: `No válido, \"<strong>${field}</strong>\" debe contener sólo letras`
    };
    if (!regexOnlyTextValidation(txt)) {
      return result;
    }
    result.status = true;
    result.onlyText = txt;
    return result;
  };
  commonFunctions.identityCheck = (identity, field) => {
    let result = {
      status: false,
      messageValidation: `No válido, \"<strong>${field}</strong>\" no es una Cédula o RUC válido`
    };
    if (!identityValidation(identity)) {
      return result;
    }
    result.status = true;
    result.messageValidation = identity;
    return result;
  };
  commonFunctions.floatDecTwoPlaces = number => {
    return (Math.round(number * 100) / 100).toFixed(2);
  };
  commonFunctions.genericRandomPassword = () => {
    const timeRnd = new Date().getTime();
    return `pass${timeRnd.toString().substr(timeRnd.toString().length - 5)}`;
  };
  commonFunctions.getStorageUrl = (value, container) => {
    const storage = process.env.AZURE_BLOB_STORAGE;
    return `${storage}${container}/${value}`;
  };
  commonFunctions.isJsonString = (str) => {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  };
  commonFunctions.isJsonStringArray = (str) => {
    try {
      const json = JSON.parse(str);
      return Array.isArray(json);
    } catch (e) {
      return false;
    }
  };
  commonFunctions.whitelistCors = () => {
    try {
      const whitelist = process.env.CORS_WHITELIST;
      return JSON.parse(whitelist);
    } catch (e) {
      return [];
    }
  };
  commonFunctions.millisecondsToHoursMinutesAndSeconds = milliseconds => {
    try {
      const seconds = (milliseconds / 1000).toFixed(1);
      const minutes = (milliseconds / (1000 * 60)).toFixed(1);
      const hours = (milliseconds / (1000 * 60 * 60)).toFixed(1);
      let hms = (hours > 0 ? hours : 0) + 'h';
      hms += ':' + (minutes > 0 ? minutes : 0) + 'm';
      hms += ':' + (seconds > 0 ? seconds: 0) + 's';
      return hms;
    } catch (e) {
      return '-';
    }
  };
  commonFunctions.identityRucValidationFinal = ruc => {
    try {
      const cad = ruc.trim();
      const two = cad.substring(0, 2);
      if (Number(two) < 1 || Number(two) > 25) {
        if (Number(two) !== 30) {
          return false;
        }
      }
      const three = cad.substring(2, 3);
      if (Number(three) < 6) {
        const last = cad.substring(10, 13);
        if (last !== '001' && last !== '002' && last !== '003') {
          return false;
        }
        return identityValidationFinal(cad.substring(0, 10));
      }
      if (Number(three) === 6) {
        const last = cad.substring(9, 13);
        if (last !== '0001' && last !== '0002' && last !== '0003') {
          return false;
        }
        const coefficient = '32765432';
        let result = 0;
        let temp = 0;
        for (let i = 0; i < coefficient.length; i += 1) {
          temp = Number(cad[i]) * Number(coefficient[i]);
          result += Number(temp);
        }
        const d = (Number(result.toString())) % 11;
        return (11 - Number(d)) === Number(cad[8]);
      }
      if (Number(three) === 9) {
        const last = cad.substring(10, 13);
        if (last !== '001' && last !== '002' && last !== '003') {
          return false;
        }
        const coefficient = '432765432';
        let result = 0;
        let temp = 0;
        for (let i = 0; i < coefficient.length; i += 1) {
          temp = Number(cad[i]) * Number(coefficient[i]);
          result += Number(temp);
        }
        const d = (Number(result.toString())) % 11;
        if (d === 0) {
          return Number(d) === Number(cad[9]);
        }
        return (11 - Number(d)) === Number(cad[9]);
      }
      return false;
    } catch (err) {
      console.log('<=== identityRucValidationFinal failed ===>', err);
    }
    return false;
  };
  commonFunctions.identityValidationFinal = identity => {
    return identityValidationFinal(identity);
  };
  commonFunctions.lookAtTerminal = async (port, db) => {
    await db.configDb.authenticate().then(() => {
      console.log(
        `-- | PostgreSQL 11 ~ connection successfully                            -- | --`
      );
    }).catch(err => {
      console.log(
        `-- | Sequelize ~ error:  ${err}                        -- | --`
      );
    });

    const localTime = myLocalTimeStr();
    const lt = upperCaseAllFirstLetter(`runtime: ${localTime}`);
    const var1 = '-- + --------------------------------------------------------------------- + --'
      .length;
    const var2 = lt.length;
    const var3 = '-- |              -- | --'.length;
    let spaces = '';
    for (let i = 0; i < var1 - (var2 + var3); i += 1) {
      spaces += ' ';
    }
  };
})(typeof exports === 'undefined' ? commonFunctions = {} : exports);
