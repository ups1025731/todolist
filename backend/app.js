const express = require('express');
const app = express();
const apolloServer = require('./graphql/apolloServerSetup');


apolloServer.applyMiddleware({ app, path: '/graphql' });

module.exports = app;


function start() {


    app.listen(3000, () =>{
        console.log('server on port', 3000)
    })
}
start();