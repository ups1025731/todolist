import DeprecatedDirective from './deprecated.directive';
import UpperCaseDirective from './uppercase.directive';

const schemaDirectives = {
    deprecated: DeprecatedDirective,
    upperCase: UpperCaseDirective
};

export default schemaDirectives;
