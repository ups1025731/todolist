import typeDefs from './schemas';
import resolvers from './resolvers';
import schemaDirectives from './directives';

const baseProcess = require('../utils/base-process');
const logger = baseProcess.logger;
const localServices = require('../services');
const {
  ApolloServer,
  ApolloError,
  UserInputError,
  ForbiddenError,
  AuthenticationError
} = require('apollo-server-express');

const authGraphql = require('./../utils/middlewares/jwt-graphql-middleware');

const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
  schemaDirectives,
  tracing: process.env.NODE_ENV === 'development',
  debug: process.env.NODE_ENV === 'development',
  introspection: process.env.NODE_ENV === 'development',
  //playground: process.env.NODE_ENV !== 'development',
  context: ({ req }) => {

    return {
      authGraphql: Object.assign({}, authGraphql(req)),
      logger,
      ApolloError,
      UserInputError,
      ForbiddenError,
      AuthenticationError
    };
  },
  dataSources: () => {
    return {
      localServices,
    };
  },
  formatError: (err) => {
    logger.error(err);
    switch (err.name) {
      case 'TokenExpiredError':
        return new ApolloError('TokenExpiredError', 401);
      default:
        if ('INTERNAL_SERVER_ERROR' === err.extensions.code) {
          return new ApolloError('playground not allowed', 401);
        } else {
          return err;
        }
    }
  }
});

module.exports = apolloServer;
