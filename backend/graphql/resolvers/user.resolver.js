import GraphQlResponse from '../models/graphql.response.model';
import UserModel from '../models/graphql.user.model';

const userResolver = {
  Query: {
    user: async (_, { uuid, codeOrigin, filter, single }, { dataSources, logger, ApolloError }) => {
      try {
        const { localServices: svc } = dataSources;
        let bodyParam = { code: codeOrigin };
        //#region "Validaciones"
        if (undefined !== filter) {
          if (undefined !== filter.name) {
            bodyParam.name = filter.name;
          }
          if (undefined !== filter.lastName) {
            bodyParam.lastName = filter.lastName;
          }
          if (undefined !== filter.role) {
            bodyParam.role = filter.role;
          }
          if (undefined !== filter.email) {
            bodyParam.email = filter.email;
          }
          if (undefined !== filter.identity) {
            bodyParam.identity = filter.identity;
          }
        } else if (undefined !== single && filter) {
          bodyParam = {};
          bodyParam.code = codeOrigin;
        }
        //#endregion
        const users = await svc.user.findUserBy(uuid, bodyParam);
        const result = {
          code: 200,
          success: true,
          message: 'OK'
        };
        if (undefined === users || users.length === 0) {
          result.code = 404;
          result.success = false;
          result.message = 'NOT_FOUND';
        } else if (users.length > 0) {
          result.userList = users;
        }
        if (undefined !== single) {
          result.user = users.find(x => x.uuid === single);
        }
        return result;
      } catch (e) {
        logger.error(e);
        throw new ApolloError('GraphQL user: error');
      }
    },
    async filterUser(root, { filter = '', limit = 10 }, { dataSources, logger, ApolloError }) {
      try {
        const usersDs = await dataSources.localServices.user.filterByName(filter, limit);
        const users = usersDs.map(r => {
          return new UserModel(r);
        });
        const data = {
          code: 200,
          success: true,
          data: users,
          totalCount: usersDs.count
        };
        return new GraphQlResponse(data);
      } catch (e) {
        logger.error(e);
        throw new ApolloError('filterUser error');
      }
    },
    userFindPersonalInfo: async (_, { uuid, filter }, { dataSources, logger, ApolloError }) => {
      try {
        const { localServices: svc } = dataSources;
        const response = { code: 200, success: true, message: 'OK' };
        const userFilter = await svc.user.svcFindUserByPersonalInfo(uuid, filter);
        if (undefined === userFilter || userFilter.length === 0) {
          response.code = 404;
          response.success = false;
          response.message = 'NOT_FOUND';
        } else {
          response.userList = userFilter;
        }
        return response;
      } catch (e) {
        logger.error(e);
        throw new ApolloError('GraphQL userFindPersonalInfo: error');
      }
    }
  },
  Mutation: {
    createUser: async (_, { user }, { authGraphql, dataSources, logger, ApolloError }) => {
      try {
        const { localServices: svc } = dataSources;
        const { uuid } = authGraphql;
        const before = await svc.user.beforeUserCreate(user.identification, user.email, user.username);
        const result = {
          code: 200,
          success: true,
          message: 'OK',
          hotel: null,
          company: null
        };
        const resultError = {
          code: 400,
          success: false,
          message: 'BAD_REQUEST',
          detail: 'Lo sentimos, hubo un problema al crear el usuario',
          user: null,
          hotel: null,
          company: null
        };
        if (undefined !== before) {
          result.success = false;
          result.detail = before.msg;
          return result;
        } else {
          const { commonFunctions: cfn } = baseProcess;
          user.pwd = cfn.genericRandomPassword();
          const createUserResult = await svc.user.createUser(uuid, user);
          if (undefined === createUserResult) {
            return resultError;
          } else {
            let title = '';
            let origin = '';
            // jshint maxdepth:5
            if (CONFIG_CODE_OWNER === user.code) {
              title += 'Bienvenido a Zigo Gestión';
              origin = 'Gestión';
            } else if (CONFIG_CODE_HOTEL === user.code) {
              title += 'Bienvenido a Zigo Hoteles';
              origin = 'Hotel';
            } else if (CONFIG_CODE_ENTERPRISE === user.code) {
              title += ' Bienvenido a Zigo Business';
              origin = 'Empresa';
            }
            user.origin = origin;
            user.platform = user.origin;
            user.roleAlias = createUserResult.roleAlias;
            dataSources.localServices.userNotification.sendUserCreationByEmail(
              EMAIL_USER_GESTION_CREATION.template,
              EMAIL_USER_GESTION_CREATION.subject,
              user
            );
            result.user = {
              id: createUserResult.idUserResult,
              uuid: createUserResult.uuid,
              username: user.username,
              name: user.firstName,
              lastName: user.firstSurname,
              email: user.email,
              identity: user.identification,
              role: user.role,
              roleAlias: createUserResult.roleAlias
            };
          }
          return result;
        }
      } catch (e) {
        logger.error(e);
        throw new ApolloError('Error al crear usuario');
      }
    },
    updateUser: async (_, { user }, { authGraphql, dataSources, logger, ApolloError }) => {
      try {
        const { localServices: svc } = dataSources;
        const { uuid } = authGraphql;
        const result = {
          code: 200,
          success: true,
          message: 'OK',
          hotel: null,
          company: null
        };
        const resultError = {
          code: 400,
          success: false,
          message: 'BAD_REQUEST',
          detail: 'Lo sentimos, hubo un problema al actualizar el usuario',
          user: null,
          hotel: null,
          company: null
        };
        const validUserBefore = await svc.user.findUserById(user.uuidUser);
        if (user.identification !== validUserBefore[0].identification) {
          const beforeUpdIdentity = await svc.user.beforeUserCreate(user.identification, null, null);
          if (undefined !== beforeUpdIdentity) {
            result.success = false;
            result.detail = beforeUpdIdentity.msg;
            return result;
          }
        }
        if (user.email !== validUserBefore[0].email) {
          const beforeUpdIdentity = await svc.user.beforeUserCreate(null, user.email, null);
          if (undefined !== beforeUpdIdentity) {
            result.success = false;
            result.detail = beforeUpdIdentity.msg;
            return result;
          }
        }
        const updateUser = await svc.user.updateUser(uuid, user);
        if (undefined === updateUser) {
          return resultError;
        } else {
          result.user = {
            id: updateUser.id,
            uuid: updateUser.uuid,
            username: updateUser.username,
            name: user.firstName,
            lastName: user.firstSurname,
            email: user.email,
            identity: user.identification,
            role: user.role,
            roleAlias: updateUser.roleAlias
          };
          const currTmpl = undefined === user.emailConfig ? EMAIL_UPDATED_GENERAL_DATA : EMAIL_UPDATED_EMAIL;
          dataSources.localServices.userNotification.sendUserUpdatedByEmail(
            currTmpl.template,
            currTmpl.subject,
            user
          );
        }
        return result;
      } catch (e) {
        logger.error(e);
        throw new ApolloError('Error al actualizar usuario');
      }
    },
  },
};
export default userResolver;
