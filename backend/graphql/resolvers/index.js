import { merge } from 'lodash';
import scalarResolver from './scalar.resolver';
import userResolver from './user.resolver';

const resolvers = merge(
  scalarResolver,
  userResolver,
);
export default resolvers;
