const { GraphQLScalarType } = require('graphql');
const { isISO8601 } = require('validator');

function parseEmail(value) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (!value || re.test(String(value).toLowerCase())) {
    return value;
  }

  try {
    if(JSON.parse(value)) {
      return value;
    }
  } catch (e) {
    throw new Error('Email Incorrecto');
  }

  throw new Error('Email Incorrecto');
}

function parsePhone(value) {
  const re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
  if (re.test(String(value).toLowerCase())) {
    return value;
  }
  throw new Error('Telefono Incorrecto');
}

const parseISO8601 = value => {
  if (isISO8601(value)) {
    return new Date(value);
  }
  throw new Error('parseISO8601: DateTime cannot represent an invalid ISO-8601 Date string');
};
const serializeISO8601 = value => {
  const valueAsString = value.toISOString();
  if (isISO8601(valueAsString)) {
    return valueAsString;
  }
  throw new Error('serializeISO8601: DateTime cannot represent an invalid ISO-8601 Date string');
};
const parseLiteralISO8601 = ast => {
  if (isISO8601(ast.value)) {
    return ast.value;
  }
  throw new Error('parseLiteralISO8601: DateTime cannot represent an invalid ISO-8601 Date string');
};

const scalarResolver = {
  GraphQlResponse: {
    __resolveType() {
      return null;
    },
  },
  Date: new GraphQLScalarType({
    name: 'Date',
    description: 'Date custom scalar type',
    serialize: serializeISO8601,
    parseValue: parseISO8601,
    parseLiteral: parseLiteralISO8601,
  }),
  Email: new GraphQLScalarType({
    name: 'Email',
    description: 'Email custom scalar type',
    parseValue: parseEmail,
    serialize: parseEmail,
    parseLiteral: ast => parseEmail(ast.value)
  }),
  Phone: new GraphQLScalarType({
    name: 'Phone',
    description: 'Phone custom scalar type',
    parseValue: parsePhone,
    serialize: parsePhone,
    parseLiteral: ast => parsePhone(ast.value)
  }),
};
export default scalarResolver;
