class GraphQlResponse {
    constructor(data) {
      const item = data ? data : {};
      this.code = item.code || null;
      this.success = typeof (item.success) === 'boolean' ? item.success : false;
      this.message = item.message || '';
      this.data = item.data || null;
      this.error = item.error || null;
      this.totalCount = item.totalCount || 0;
    }
  }
  export default GraphQlResponse;
  