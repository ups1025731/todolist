class UserModel {

    constructor(data) {
  
  
      const item = data ? data : {};
  
      this.id = item.id || null;
      this.uuid = item.uuid || null;
      this.username = item.username || '';
      this.name = item.first_name || '';
      this.lastName = item.first_surname || '';
      this.email = item.email || '';
      this.indentity = item.identification || '';
    }
  
  }
  
  export default UserModel;
  