import schema from './schema.graphql';
import scalar from './scalar.graphql';
import enums from './enums.graphql';
import directives from './directives.graphql';
import interfaces from './interfaces.graphql';
import Query from './query.graphql';
import Mutation from './mutation.graphql';
import Filter from './types/filter.graphql';
import User from './types/user.graphql';

const typeDefs = [
  Filter,
  schema,
  scalar,
  enums,
  directives,
  interfaces,
  Query,
  Mutation,
  User,
];
module.exports = typeDefs;